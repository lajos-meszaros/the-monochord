# The Monochord

This is the backend and frontend code for the microtonal web app [The Monochord](http://the-monochord.com)

The project is maintained by Lajos Mészáros (m_lajos[at]hotmail[dot]com)
